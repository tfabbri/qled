CONFIG      += designer plugin debug_and_release
TARGET      = $$qtLibraryTarget($$TARGET)
TEMPLATE    = lib
QT += svg

QTDIR_build:DESTDIR     = $$QT_BUILD_TREE/plugins/designer

HEADERS     = qled.h \
              qledplugin.h
SOURCES     = qled.cpp \
              qledplugin.cpp

RESOURCES = qled.qrc

QLEDHEADER = qled.h


# install
target.path = $$[QT_INSTALL_PLUGINS]/designer
sources.files = $$SOURCES $$HEADERS *.pro
sources.path = $$[QT_INSTALL_EXAMPLES]/designer/qledplugin

qledinclude.files = $$QLEDHEADER
qledinclude.path = /usr/local/include/

qledlib.files = *.so
qledlib.path = /usr/local/lib/ 

INSTALLS += target sources qledinclude qledlib
